var
    math = $2D.physics.Math,
    Circle = function (x, y, r, color, player) {
        this.body = $2D.Body($2D.Circle(x, y, r));
        this.pos = this.body.get('pos');
        this.size = {r: r};

        this.pixi = new PIXI.Graphics();
        this.pixi.beginFill(color);
        this.pixi.drawCircle(0, 0, r);
        this.pixi.endFill();
        //this.pixi.position.x = x;
        //this.pixi.position.y = y;

        if(player)
            this.player = true;
    },

    Rect = function (x, y, w, h, color, player) {
        this.body = $2D.Body($2D.Rect(x, y, w, h));
        this.pos = this.body.get('pos');
        this.size = {w: w, h: h};

        this.pixi = new PIXI.Graphics();
        this.pixi.beginFill(color);
        this.pixi.drawRect(0, 0, w, h);
        this.pixi.endFill();
        //this.pixi.position.x = x;
        //this.pixi.position.y = y;
        this.player = false;

        if(player)
            this.player = true;
    }

//Setup
$2D.setup(function() {
    this.screen     = {w: 1800, h: 900};
    this.renderer   = new PIXI.WebGLRenderer(this.screen.w, this.screen.h, {backgroundColor: 0x000000, antialias: true});
    this.mouse  = [];
    this.size = 20;
    this.rects = [];

    /*for(var i = 0; i < 2; i++) {
        var size = Math.floor((Math.random() * 3) + 3);
        this.rects[i] = new Rect(
            this.size * Math.floor((Math.random() * 85) + 1),
            this.size * Math.floor((Math.random() * 37) + 1),
            10 * size,
            10 * size,
            0x5ccdc9
        );
        this.rects[i].body.set('vel', $2D.Vector(0.1,0))
    }*/


    this.rects[0] = new Rect(0,0,100,100,0x5ccdc9);
    this.rects[1] = new Rect(300,300,100,100,0x5ccdc9);
    this.rects[2] = new Rect(500,500,100,100,0x5ccdc9);

    $r (document).on('mousemove', $func (function(e) {
        this.mouse = $doc (document).mousePosition(e);
    }).bind(this));

    $r('#screen').el.appendChild(this.renderer.view);

    this.seed = Math.random();
    noise.seed(this.seed);

    this.spatial = $2D.Spatial(this.screen.w, this.screen.h, 100);
     this.field  = $2D.FlowGrid(this.screen.w, this.screen.h, 20, $func (function(x, y){
         noise.seed(this.seed);
         var theta = math.map(noise.simplex2(x,y), 0, 1, 0, (2 * Math.PI));
         return $2D.Vector(math.cos(theta), math.sin(theta));
     }).bind(this));
    this.field.generate();


}, {stats: true, type: 0});

//Run
$2D.run(function() {
    var stage = new PIXI.Container(),
        angle = $2D.Angles.toRadians(45);

    this.spatial.generate();
    for(var id in rects) {
        var obj = rects[id],
            pos = obj.pos,
            m = new Matrix();

        m.rotate(-angle);

        console.log(m.elements);


        this.spatial.insert(new $2D.Rect(m.elements[2], math.round(-m.elements[5]), obj.size.w, obj.size.h), obj);
        //console.log(pos);
    }

    for(var id in rects) {
        var obj     = rects[id],
            pos     = obj.pos,
            others  = this.spatial.retrieve($2D.Rect(pos.x, pos.y, obj.size.w, obj.size.h), obj),
            steer   = $2D.Steering(obj.body, {seek: 1, wander: 0.5, separation: 1, align: 0.7, cohesion: 1});


        //steer.wander({radius: math.cos(math.map(noise.simplex2(obj.pos.x, obj.pos.y), 0, 1, 0, (2 * Math.PI)))});
        //steer.flow(this.field);
        //steer.separation(rects);
        //steer.seek($2D.Vector(this.mouse.x, this.mouse.y));
        if(others.length > 0) {
           //steer.align(others);
           //steer.separation(others);
           //steer.cohesion(others);
        }
        steer.apply();
    }


    var lines = this.spatial.render();
    stage.addChild(lines);
    for(var id in rects) {
        var obj = rects[id];
        obj.body.spawn();
        //obj.pixi.position.x = obj.pos.x;
        //obj.pixi.position.y = obj.pos.y;

        console.log(obj.pixi);
        obj.pixi.rotation = angle;
        stage.addChild(obj.pixi);

    }
    renderer.render(stage);
});