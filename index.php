<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Entity Framework</title>
        <style>
			body {
				background: #333333;
			}
		</style>
    </head>
    
    <body>

    <div id="screen">

    </div>

    <!------------ Libraries ------------>
    <!-- RuddyJS v0.2 -->
    <script src="../RuddyJS v0.2/globals/core.js"></script>
    <script src="../RuddyJS v0.2/globals/object.js"></script>
    <script src="../RuddyJS v0.2/globals/function.js"></script>
    <script src="../RuddyJS v0.2/globals/array.js"></script>
    <script src="../RuddyJS v0.2/globals/string.js"></script>
    <script src="../RuddyJS v0.2/globals/element.js"></script>
    <script src="../RuddyJS v0.2/globals/nodes.js"></script>
    <script src="../RuddyJS v0.2/globals/document.js"></script>
    <script src="../RuddyJS v0.2/globals/style.js"></script>
    <script src="../RuddyJS v0.2/events/input.js"></script>
    <script src="../RuddyJS v0.2/ruddy.js"></script>
    <script src="../RuddyJS v0.2/ext/ajax.js"></script>
    <script src="../RuddyJS v0.2/ext/animation.js"></script>
    <script src="../RuddyJS v0.2/ext/scroll.js"></script>

    <!-- Extra -->
    <script src="scripts/lib/pixi.min.js"></script>
    <script src="scripts/lib/stats.min.js"></script>
    <script src="scripts/lib/json3.min.js"></script>

    <!------------ Ruddy2D ------------>
    <!-- Engine -->
    <script src="scripts/engine/ruddy2D.js"></script>
    <script src="scripts/engine/settings.engine.js"></script>
    <script src="scripts/engine/user.engine.js"></script>
    <script src="scripts/engine/physics.engine.js"></script>
    <script src="scripts/engine/entity.engine.js"></script>
    <script src="scripts/engine/components.engine.js"></script>
    <script src="scripts/engine/assemblages.engine.js"></script>

    <!-- Physics -->
    <!-- Utilities -->
    <script src="scripts/engine/physics/util/math.js"></script>
    <script src="scripts/engine/physics/util/angles.js"></script>
    <script src="scripts/engine/physics/util/heuristic.js"></script>

    <!-- Math -->
    <script src="scripts/engine/physics/math/vector.js"></script>
    <script src="scripts/engine/physics/math/node.js"></script>
    <script src="scripts/engine/physics/math/force.js"></script>
    <script src="scripts/engine/physics/math/body.js"></script>
    <script src="scripts/engine/physics/math/collision.js"></script>

    <!-- Geometries -->
    <script src="scripts/engine/physics/geometries/circle.js"></script>
    <script src="scripts/engine/physics/geometries/rectangle.js"></script>

    <!-- Forces -->
    <script src="scripts/engine/physics/forces/gravity.js"></script>
    <script src="scripts/engine/physics/forces/attraction.js"></script>
    <script src="scripts/engine/physics/forces/friction.js"></script>
    <script src="scripts/engine/physics/forces/drag.js"></script>
    <script src="scripts/engine/physics/forces/pendulum.js"></script>
    <script src="scripts/engine/physics/forces/steering/seek.js"></script>
    <script src="scripts/engine/physics/forces/steering/arrival.js"></script>
    <script src="scripts/engine/physics/forces/steering/flow.js"></script>

    <!-- Algorithms -->
    <script src="scripts/engine/physics/algorithms/spatialGrid.js"></script>
    <script src="scripts/engine/physics/algorithms/grid.js"></script>
    <script src="scripts/engine/physics/algorithms/clearance.js"></script>
    <script src="scripts/engine/physics/algorithms/binaryHeap.js"></script>
    <script src="scripts/engine/physics/algorithms/find/aStar.js"></script>
    <script src="scripts/engine/physics/algorithms/find/dijkstra.js"></script>
    <script src="scripts/engine/physics/algorithms/follow/simple.js"></script>

    <!-- Behaviors -->
    <script src="scripts/engine/physics/behaviors/pathFinding.js"></script>
    <script src="scripts/engine/physics/extend.js"></script>

    <!-- Test -->
    <script src="scripts/data/worlds/main.world.js"></script>
    </body>
</html>
